//
//  ViewController.m
//  UIele
//
//  Created by Clicklabs 104 on 9/18/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
UITextView *textview;
UISlider *slider;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    textview= [[UITextView alloc] initWithFrame:CGRectMake(70, 300, 200, 40)];
    textview.text= @"text";
    textview.textColor= [UIColor whiteColor];
    textview.backgroundColor= [UIColor whiteColor];
    [self.view addSubview: textview];
    
    slider= [[UISlider alloc] initWithFrame:CGRectMake(70, 500, 200, 40)];
    slider.backgroundColor=[UIColor blueColor];
    [slider setMaximumValue:100];
    [slider setMinimumValue:0];
    [slider addTarget: self action : @selector(valuechanged:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:slider];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}
-(void) valuechanged: (UISlider *) sender{
    textview.text= [NSString stringWithFormat:@"%.0f", sender.value];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
